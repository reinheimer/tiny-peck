
document.addEventListener('DOMContentLoaded', function () {
    // alert("oi");
    // chrome.tabs.getCurrent(function callback)
    // chrome.tabs.query(object queryInfo, function callback)

    chrome.tabs.query({ active: true }, function (tabs) {

        // alert("popup.js says:\n\t" + tabs.length + " tab retornada");
        // alert(JSON.stringify(tabs[0]));
        // chrome.runtime.sendMessage(string extensionId, any message, object options, function responseCallback)
        chrome.runtime.sendMessage({ getSuggestions: true, senderTabId: tabs[0].id }, function (response) {
            // alert("popup.js says: \n" + JSON.stringify(response));

            if (response.items == 0) {
                // mostrar mensagem que não há sugestões
                var par1 = document.createElement('h2');
                var line1 = document.createTextNode("Ainda não há sugestões");
                par1.appendChild(line1);

                var par2 = document.createElement('h3');
                var line2 = document.createTextNode("pois elas se baseiam no relacionamento entre favoritos");
                par2.appendChild(line2);
                
                var popup = document.getElementById('related');
                popup.appendChild(par1);
                popup.appendChild(par2);
            }
            else {
                var desc1 = document.createElement('h2');
                var desc2 = document.createElement('small');
                var desc3 = document.createTextNode("Favoritos relacionados sugeridos:");
                desc2.appendChild(desc3);
                desc1.appendChild(desc2);
                document.getElementById('related').appendChild(desc1);

                response.suggestions.forEach(function(related) {
                    chrome.bookmarks.get(related.favId, function(result) {

                        var icone= document.createElement("img");
                        icone.setAttribute("src", "chrome://favicon/" + result[0].url);
                        icone.setAttribute("height", '16px');
                        icone.setAttribute("width", '16px');

                        var newlink = document.createElement('a');
                        newlink.setAttribute('href', result[0].url);
                        newlink.setAttribute('target', "_blank");
                        var text = document.createTextNode(result[0].title);
                        newlink.appendChild(text);
                        var listItem = document.createElement('li');
                        listItem.appendChild(icone);
                        listItem.appendChild(newlink);
                        document.getElementById('related').appendChild(listItem);



                    });
                });
            }
        });
    });

    
  
});
