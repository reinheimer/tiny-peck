var minhocas = {};

function alertFunc() {

    var conn;

    // alert(x + ' × ' + y);

    jsPlumb.ready(function () {

        // setup some defaults for jsPlumb.
        instance = jsPlumb.getInstance({
            Endpoint: ["Dot", {radius: 2}],
            HoverPaintStyle: {strokeStyle: "#FF4747", lineWidth: 2 },
            //Container: "lombrigas"
            //Container: "mytimeline"
            //Container: "myfore"
            Container: "mytime"
        });

        window.jsp = instance;
        //window.jsp = document.getElementById('mytimeline');

        var windows = jsPlumb.getSelector(".statemachine-demo .w");
        //var windows = jsPlumb.getSelector(".vis.timeline .item");

        /*### não quero arrastar os favoritos ###*
        // initialise draggable elements.
        instance.draggable(windows);
        */
        //instance.draggable(jsPlumb.getSelector("#lombrigas"), { containment: true });

        //chrome.storage.sync.clear();

        instance.bind("click", function (c) {
            instance.detach(c);
        });

        instance.bind("connection", function(info, originalEvent) {
            
            var key = info.connection.id;

            // console.log("S: " + info.sourceId + " - T: " + info.targetId);

            var connData = {
                source: info.sourceId, 
                target: info.targetId,
                parameters: { "id": key} 
            };
            
            // Object literal - serialized as empty when syncing
            //minhocas[key] = JSON.stringify(connData);
            minhocas[key] = connData;

            //info.connection['parameters'] = {'id': key};

            if (typeof originalEvent === 'undefined') {
                // conexão sendo criada via código
                //info.connection.getOverlay("label").setLabel(info.connection.id);
            } else {
                // atualizar o storage
                
                // console.log(JSON.stringify(info.connection.getParameters()));                
                // console.log('minhocas: ' + JSON.stringify(minhocas));

                chrome.storage.sync.set({'connections': JSON.stringify(minhocas)}, function() {
                    console.log("Relação " + JSON.stringify(minhocas[key]) + " salva! ");
                    // console.log("{'connections': " + JSON.stringify(minhocas) + "}");
                });
            }
        });

        instance.bind("connectionDetached", function(info, originalEvent) {
        //instance.bind("click", function(info, originalEvent) {
            // Esta acaontecendo algumas coisas estranhas, nao esta funcionando para toodos os casos
           // alert(JSON.stringify(info.connection.getParameter("id")) + " id:" + info.connection.id);

            var removeId;

            if (info.connection.getParameter("id") == undefined) {

                // tela ainda não foi recarregada desde a criação da conexão, ou seja, não tem perigo de ela ter recibido outro id!
                // console.warn("Não foi possivel remover no momento\n\t\tParâmetro conn_id não está preenchido");
                removeId = info.connection.id;
                //location.reload();

            } else {

                removeId = info.connection.getParameter("id"); // ai ai
            }

            minhocas[removeId] = null;
            delete minhocas[removeId]; // não sei se é a forma correta de remover um 
                                            // campo de um objeto, mas foi a única que encontrei
            chrome.storage.sync.set({'connections': JSON.stringify(minhocas)}, function() {
                if (!chrome.runtime.lastError)
                    console.log("Relação removida");

            });

            //chrome.storage.sync.remove(removeId, function() {
                // alert("Relação " + removeId + " removida!");
            //})
        });

        // suspend drawing and initialise.
        instance.batch(function () {
            instance.makeSource(windows, {
                filter: ".ep",
                anchor:[ "TopCenter","RightMiddle","BottomCenter","LeftMiddle" ],
                connector: [ "Bezier", { curviness: 50 } ],
                connectorStyle: { strokeStyle: "#5c96bc", lineWidth: 2, outlineColor: "transparent", outlineWidth: 4 },
                maxConnections: 5,
                onMaxConnections: function (info, e) {
                    alert("Maximum connections (" + info.maxConnections + ") reached");
                }
            });

            // initialise all '.w' elements as connection targets.
            instance.makeTarget(windows, {
                dropOptions: { hoverClass: "dragHover" },
                anchor:[ "TopCenter","RightMiddle","BottomCenter","LeftMiddle" ],
                allowLoopback: false
            });

            // recupera todas as relações do Storage e desenha na timeline
            chrome.storage.sync.get('connections', function(myConnections) {

                //console.log('myConnections = ' + JSON.stringify(myConnections));

                if (JSON.stringify(myConnections) !== '{}') {

                    // console.log('connections = ' + JSON.stringify(myConnections.connections));

                    var aux = JSON.parse(myConnections.connections);

                    //var newArray = JSON.parse(aux);
                    //var newArray = [];
                    //newArray.push(aux[0]);
                    //newArray.push(aux[1]);

                    // console.log('newArray = ' + JSON.stringify(newArray));

                    // console.log('connections = ' + JSON.stringify(aux));

                    var connectionKeys = Object.keys(aux);

                    //console.log('Connection Keys: ' + connectionKeys);
                    
                    connectionKeys.forEach(function(connId) {

                        var conn = instance.connect(aux[connId]);
                    });

                }
            });
        });

        timeline.on("rangechange", function () {
            instance.batch(function () {
                //instance.recalculateOffsets("lombrigas");
                instance.repaint("lombrigas");
            });
        });

        // retamanhar janela
        window.addEventListener("resize", function () {
            instance.batch(function () {
                //instance.recalculateOffsets("lombrigas");
                instance.repaint("lombrigas");
            });
        });
        // timeline.on("", function () {});

        //timeline.redraw();

        instance.bind("connectionMoved", function(info, originalEvent) {
            alert(info.newSourceId);
        });

        // guardar as conexões em memória
        /*connections = [];

        instance.getConnections().forEach( function (connection)  {
            connections.push({
                connectionId: connection.id,
                pageSourceId: connection.sourceId,
                pageTargetId: connection.targetId

            });
        }); não lembro porque preciso disso */

        jsPlumb.fire("jsPlumbDemoLoaded", instance);

    });

}