//alert("Ok");
// array de sugestoes
//var suggestions = {};
var suggestions = {};
var thumbnails = {};

chrome.browserAction.setBadgeBackgroundColor({ color: [122, 176, 44, 255]});

// ouvir quando um novo favorito é criado e usar
// chrome.tabs.captureVisibleTab(integer windowId, object options, function callback)

function getSuggestions() {
	return suggestions;
}

// queria que, sempre ao clicar no botão da extensão, a lista de sugestões fosse atualizada mas
// "This event will not fire if the browser action has a popup." - https://developer.chrome.com/extensions/browserAction#event-onClicked
chrome.browserAction.onClicked.addListener(function(tab) {
	chrome.bookmarks.search({ 'url': tab.url }, 
    	function(results) { 
    		// jah eh favorito (achar um jeito melhor de descobrir!)
    		if (results !== null && results.length) {
    			generateSuggestions(results[0].id, tab.id);
   			}
   		}
    );
});

// quando uma aba é atualizada // está sendo chamado mais de uma vez quando um favorito é acessado. talvez setBadge() faça update da tab?
chrome.tabs.onUpdated.addListener(

  // vemos se o URL mudou. Callback retorna integer tabId, object changeInfo, Tab tab
  function(tabId, changeInfo, tab) {

	if (changeInfo.url) {
		// alert("tabId = " + tabId + "\ntab.id = " + tab.id); // sempre iguais?

    	chrome.bookmarks.search({url: changeInfo.url}, function(results) { 
    			// jah eh favorito (achar um jeito melhor de descobrir!)
    			if (results !== null && results.length) {
    				takeScreenshot(results[0].id);
    				generateSuggestions(results[0].id, tab.id);
    				// generateSuggestions() tem chamada assíncrona, não adianta testar o array de sugestões aqui
    			} else {
    				delete suggestions[tab.id]; // não é favorito, tira do array de tabs com sugestão
    			}
    		}
    	);
    }
    //chrome.bookmarks
	//alert("Nova URL: " + changeInfo.url);
  }
);

/*function generateSuggestions(id, tabId) {

	// recupera todas as relações do Storage e desenha na timeline
	chrome.storage.sync.get(null, function(myConnections) {
		
		// retorna um array com as chaves do dicionário que guarda as relações
		var connectionKeys = Object.keys(myConnections);

        var initialNode, graph;

        while(connectionKeys.length > 0) {
        	// retirar um id de relação do array
        	var connId = connectionKeys.pop();
        	
        	var conn = JSON.parse(myConnections[connId]);

        	if (conn.source == "bookmark" + id) {

        		// adiciona nodo vizinho ao grafo
        		var neighbor = {favId: conn.target.slice(8), aff: 0, neig: []};
        		graph[neighbor.favId] = neighbor;

				// cria o nodo inicial se ele for nulo
				if (initialNode === undefined) {
					initialNode = {favId: id, aff: -1000, neig: [neighbor]};
				} else { // se não, adiciona o vizinho
					initialNode.neig.push(neighbor);
				}

			} else if (conn.target == "bookmark" + id) {

				// adiciona nodo vizinho ao grafo
				var neighbor = { favId: conn.source.slice(8), aff: 0, neig: [] };
				graph[neighbor.favId] = neighbor;

				// cria nodo inicial se ele for nulo
				if (initialNode === undefined) {
					initialNode = { favId: id, aff: -1000, neig: [neighbor] };
				} else { // se não, adiciona o vizinho na lista dele
					initialNode.neig.push(neighbor);
				}

			} else { 

				// nodo não é o inicial
				var tempSource, tempTarget;
				var sourceId = conn.source.slice(8);
				var targetId = conn.target.slice(8);

				// ver se o nodo já existe
				if (graph[sourceId] === undefined) {
					tempSource = { favId: sourceId, aff: 0, neig: [] };
				} else {
					tempSource = graph[sourceId];
				}

				// ver se o nodo já existe
				if (graph[targetId] === undefined) {
					tempTarget = { favId: targetId, aff: 0, neig: [] };
				} else {
					tempTarget = graph[targetId];
				}

				tempSource.neig.push(tempTarget);
				tempTarget.neig.push(tempSource);
			}

        }

        suggestions = suggest(graph, initialNode);

		//suggestions = initialNode.neig.slice(0, 3);
		suggestions = initialNode.neig;

		if (suggestions.length > 0) 
			chrome.browserAction.setBadgeText({text: JSON.stringify(suggestions.length), tabId: tabId});

	});
}*/

/*
** Recebe o id do favorito a partir do qual é necessário gerar sugestões na aba indicada por tabId
*/
function generateSuggestions(id, tabId) {

	var initialNode = {favId: id, neig: []};
	var connectionKeys, connections;

	// recupera todas as relações do Storage
	chrome.storage.sync.get('connections', function(myConnections) {
                
		if (JSON.stringify(myConnections) != '{}') {

			connections = JSON.parse(myConnections.connections);

		 	connectionKeys = Object.keys(connections);

			connectionKeys.forEach(function(connId) {
				
				var conn = connections[connId];

				if (conn.source == "bookmark" + id) {

					initialNode.neig.push({favId: conn.target.slice(8), aff: 0, neig: []});
				
				} else if (conn.target == "bookmark" + id) {
					
					initialNode.neig.push({favId: conn.source.slice(8), aff: 0, neig: []});

				}

			});

			// alert("Vizinhos = " + initialNode.neig.length);

			// suggestions[tabId] = initialNode.neig.slice(0, 3);
			suggestions[tabId] = initialNode.neig.slice(0, 3);

			// console.log("suggestions[" + tabId + "] = " + JSON.stringify(suggestions[tabId]));

			suggestions[tabId].forEach(function (s) {
				// alert("suggestions.js says:\n\ts.favId = " + s.favId);
				s.thumbnail = thumbnails[s.favId];
			});

			console.log("suggestions[" + tabId + "] = " + JSON.stringify(suggestions[tabId]));

			// alert(JSON.stringify(suggestions));

			// alert("Sugestões = " + suggestions.length);
			// suggestions[tabId] = initialNode.neig;

			/*if (suggestions[tabId].length > 0) {
				chrome.browserAction.setBadgeText({text: JSON.stringify(suggestions[tabId].length), tabId: tabId});
				chrome.browserAction.setBadgeBackgroundColor({ color: [122, 176, 44, 255]});
			}*/
			if (suggestions[tabId].length > 0) {
				chrome.browserAction.setBadgeText({'text': JSON.stringify(suggestions[tabId].length), 'tabId': tabId});
			}
			//return suggestions.length;
		}
	});
}

function suggest(graph, initialNode) {
	
	if (graph == null || initialNode == null) return;

	var threeMoreAkin = [];
	var lowestAff = -1000;

	initialNode.neig.forEach(function(neighbor) {
		if (neighbor.aff >= lowestAff) {
			threeMoreAkin.push(neighbor);
			lowestAff = neighbor.aff;
		}
	});
}

/*
chrome.browserAction.onClicked.addListener(function(tab) {

	//alert("Tamanho do array de sugestões: " + getSuggestions().length);
	suggestions.forEach(function(related) {

    	//alert("Tamanho do array de sugestões: " + related.length);
    	
    	chrome.bookmarks.get(related.id, function(result) {

	      	//alert("URL da sugestão:\n" + result.url);

	      	var newlink = document.createElement('a');
	      	newlink.setAttribute('href', result.url);
	      	var text = document.createTextNode(result.title);
	      	newlink.appendChild(text);
	      	var divQuerida = document.getElementById('related');
	      	divQuerida.appendChild(newlink);
	      	//this.appendChild(newlink);
    	});
  });
//  chrome.browserAction.setBadgeText({text: "1", tabId: tab.id});
//  chrome.tabs.create({'url': chrome.extension.getURL('index.html')}, function(tab) {
      // Tab opened.
//  });
});*/

function takeScreenshot(favId) {
    chrome.tabs.captureVisibleTab({ format: "png" }, function (dataUrl) {
        // console.log(JSON.stringify(dataUrl));
        thumbnails[favId] = dataUrl;
        // alert("suggestions.js says:\n\tthumbnails[favId] = " + thumbnails[favId]);
    });
}

//chrome.runtime.onMessage.addListener(function(any message, MessageSender sender, function sendResponse) {...};)
chrome.runtime.onMessage.addListener(
	function(message, sender, sendResponse) {

		if (message.getSuggestions) {
			var tabId = message.senderTabId;
			sendResponse({ 'items': suggestions[tabId].length, 'suggestions': suggestions[tabId] });
			//alert("suggestions.js says:\n\tmessage.senderTabId = " + tabId);
			//alert("suggestions.js says:\n\tsuggestions[" + tabId + "] = " + JSON.stringify(suggestions[tabId]));
		}
		
		// alert("Message received: \n\t" + JSON.stringify(message) + "\nfrom\n\t" + JSON.stringify(sender));
		// alert("Message received: \n\t" + JSON.stringify(message));

		//if (message.getSuggestions)
		//	console.log("oi");
		//else
		//	console.log("oi2");

		//if (JSON.parse(message).getSuggestions) {
			//sendResponse({items: suggestions[sender.tab].length, suggestions: suggestions[sender.tab]});
		//}

	}
);

// quando uma aba é fechada? remover suas sugestões? atualizar com o tempo?

