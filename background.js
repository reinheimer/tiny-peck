var timeline;
var categories;
var data;
var instance;

//chrome.browserAction.onClicked.addListener(function(tab) {
//  chrome.browserAction.setBadgeText({text: "1", tabId: tab.id});
//  chrome.tabs.create({'url': chrome.extension.getURL('index.html')}, function(tab) {
      // Tab opened.
//  });
//});

function init(bookmarksCount) {

    /* acesso realizado */
    updateAccessData(bookmarksCount);

    // ajustar o tamanho do rodapé
    adjustTimelineHeight();

    /** event listeners **/

    window.onresize = adjustTimelineHeight;

    document.getElementById('showReport').onclick = showReport;

    document.getElementById('selecao').addEventListener("change", novaOrdenacao);

    //document.getElementById('myHelp').onfocus = alert("done");

}

function adjustTimelineHeight() {
    var w = window,
        d = document,
        e = d.documentElement,
        g = d.getElementsByTagName('body')[0],
        x = w.innerWidth || e.clientWidth || g.clientWidth,
        y = w.innerHeight|| e.clientHeight|| g.clientHeight;

    var tlElem = document.getElementById('mytimeline');

    tlElem.style.height = (y - 100) + "px";

}

function drawSample() {

    var dLineY = 40;
    var lineY = 10;
    var x = 9;

    var time = new Date();
    // var x = time.getMilliseconds() * 0.4;
    var milisec = time.getMilliseconds();
    milisec *= 3;
    var maxMilisec = 999 * 3;
    var minMilisec = 0;
    var maxX = 300;
    var minX = -200;
    //var x = (milisec - minMilisec) / (maxMilisec - minMilisec) * (maxX - minX) + minX;
    // console.log("x = " + x);
    // var x = time.getMilliseconds() * 0.4 - 100;

    // http://www.w3schools.com/html/html5_canvas.asp
    var ctx = document.getElementById("myCanvas").getContext("2d");

    // ctx.clearRect(0, 0, 100, 200); // deixo isso aqui pela risada
    ctx.clearRect(0, 0, 200, 100);

    ctx.beginPath();

    ctx.strokeStyle = "#EEEEEE";
    
    ctx.moveTo(0, lineY);
    ctx.lineTo(200, lineY);

    lineY += dLineY;

    ctx.moveTo(0, lineY);
    ctx.lineTo(200, lineY);

    lineY += dLineY;

    ctx.moveTo(0, lineY);
    ctx.lineTo(200, lineY);
    
    ctx.stroke();

    //ctx.save();

    // desenha primeiro bookmark
    ctx.fillStyle = "#DEDEDE";
    ctx.fillRect(x, 17, 84, 26);

    // cursor fica 250 milissegundos sobre o endpoint e mais 250 sobre o segundo bookmark
    var cursorX = 75;
    var cursorY = 27;
    if (milisec > 250 * 3) {
        cursorX = (milisec - minMilisec) / (maxMilisec - minMilisec) * (110 - 75) + 75;
        cursorY = (milisec - minMilisec) / (maxMilisec - minMilisec) * (64 - 27) + 27;
    } else if (milisec > 750 * 3) {
        cursorX = 110;
        cursorY = 64;
    }
    
    ctx.beginPath(); // aquilo que você não sabe volta para te assombrar

    // minhoca
    ctx.strokeStyle = "#5C96BC";
    ctx.lineWidth = 2;
    ctx.moveTo(x + 66, 18 + 26 / 2 - 5);
    ctx.lineTo(cursorX + 4, cursorY + 4);
    ctx.stroke();


    // endpoint selecionado
    if (cursorX < 80 && cursorY < 30) {
        ctx.fillStyle = "#4A3939";
        ctx.fillRect(x + 64, 17 + 26 / 2 - 6, 12, 12);
    }

    // segundo bookmark selecionado
    if (cursorX > 108 && cursorY > 60) {
        ctx.fillStyle = "#4A3939";
        ctx.fillRect(x + 100, 16 + 40, 86, 28);
    }

    // desenha segundo bookmark
    ctx.fillStyle = "#DEDEDE";
    ctx.fillRect(x + 101, 17 + 40, 84, 26);

    // endpoints
    ctx.fillStyle = "#FF920B";
    ctx.fillRect(x + 65, 17 + 26 / 2 - 5, 10, 10);
    ctx.fillRect(x + 101 + 65, (17 + 40) + 26 / 2 - 5, 10, 10);

    // desenha cursor
    var cursor = new Image();
    cursor.src = 'http://www.snazzyspace.com/cursorsfolder/mickey-pointer.png';
    // começa no [75, 27]
    // vai até [110, 64]
    ctx.drawImage(cursor, cursorX, cursorY);
}

function updateAccessData(bookmarksCount) {

    chrome.storage.sync.get('access_count', function(report) {
        // console.log("Relatório de acessos: " + JSON.stringify(report.access_count));

        var access_count;

        /*var dialog = document.getElementById("myHelp");
        dialog.showModal(); 
        //drawSample();
        setInterval(drawSample, 5);*/

        // console.log(JSON.stringify(report));

        if (JSON.stringify(report) == '{}' && bookmarksCount > 0) {
            var dialog = document.getElementById("myHelp");

            setTimeout(function() { 
                dialog.showModal(); 
                setInterval(drawSample, 3);
            }, 1500);
        //}

        //if (report.length == 0 || report.access_count === undefined) {
            access_count = { daily: 1, monthly: 1, annually: 1, last_access: JSON.stringify(new Date()) };
            
            chrome.storage.sync.set({'access_count': access_count}, function() {
                if (!chrome.runtime.lastError) {
                    console.log('Dados de acesso criados: ' + JSON.stringify(access_count));
                } else {
                    console.warn('Não foi possível criar os dados de acesso\n\t\t' + chrome.runtime.lastError.message);
                }
            });
            
        } else {

            access_count = report.access_count;

            var last_access = new Date(JSON.parse(access_count.last_access));
            //var last_access = new Date();
            // console.log('Last access: ' + last_access);

            var today = new Date();
            // console.log('Today: ' + today);

            if (last_access.getFullYear() != today.getFullYear()) {
                
                access_count['daily']       = 1;
                access_count['monthly']     = 1;
                access_count['annually']    = 1;
            
            } else if (last_access.getMonth() != today.getMonth()) { // getMonth() retorna de 0 a 11

                access_count['daily']       = 1;
                access_count['monthly']     = 1;
                access_count['annually']    = parseInt(access_count['annually']) + 1;
           
            } else if (last_access.getDate() != today.getDate()) {

                access_count['daily']       = 1;
                access_count['monthly']     = parseInt(access_count['monthly']) + 1;
                access_count['annually']    = parseInt(access_count['annually']) + 1;
            
            } else {
                console.log('Same daY!');
                access_count['daily']       = parseInt(access_count['daily']) + 1;
                access_count['monthly']     = parseInt(access_count['monthly']) + 1;
                access_count['annually']    = parseInt(access_count['annually']) + 1;

            }

            //console.log('Está guardando isso: ' + today.toJSON().slice(0, 10));
            //console.log(JSON.stringify(today));

            access_count['last_access']     = JSON.stringify(today);

            chrome.storage.sync.set({'access_count': access_count}, function() {
                if (!chrome.runtime.lastError) {
                    console.log('Dados de acesso atualizados: ' + JSON.stringify(access_count));
                } else {
                    console.warn('Não foi possível atualizar os dados de acesso\n\t\t' + chrome.runtime.lastError.message);
                }
            });
        }
    });

}

document.addEventListener('DOMContentLoaded', function () {

    chrome.tabs.captureVisibleTab({ format: "png" }, function (dataUrl) {
        console.log(JSON.stringify(dataUrl));
    });

    var result = new Array(0);

    chrome.bookmarks.getTree(function (results) {
    
        result = addTreeNodeToTimeline(results[0], "");

        init(result.bookmarks.length);
        //window.alert("Quantidade de favoritos encontrados: " + result.bookmarks.length);

        // DOM element where the Timeline will be attached
 
        var container = document.getElementById('mytimeline');

        if (result.bookmarks.length == 0) {

            // Silver
            container.style.backgroundColor = "Gainsboro";

            var earlyDiv = document.createElement("div");
            earlyDiv.id = "earlyNotice";

            var topParagraph = document.createElement("h1");
            topParagraph.appendChild(document.createTextNode("Parece que chegamos cedo demais!"));
            //topParagraph.className = "";

            earlyDiv.appendChild(topParagraph);
            
            var img = document.createElement("img");
            img.src = "src/images/egg-128.png";
            earlyDiv.appendChild(img);

            var botParagraph = document.createElement("h2");
            botParagraph.appendChild(document.createTextNode("Você ainda não salvou favoritos"));
            
            earlyDiv.appendChild(botParagraph);

            container.appendChild(earlyDiv);

            document.getElementById("selecao").disabled = true;

        } else {

            // Create a DataSet with data (enables two way data binding)
            data = new vis.DataSet(result.bookmarks);
            categories = result.categories;

            var maxMoment = new Date();
            maxMoment.setDate(maxMoment.getDate() + 10);

            var minMoment = result.oldestBookmark;

            // console.log(minMoment);
            minMoment.setDate(minMoment.getDate() - 10);

            // console.log(minMoment);
            var otherDate = new Date();
            otherDate.setDate(maxMoment.getDate() - 30);
            // Configuration for the Timeline
            var options = {
                width: '100%',
                //maxHeight: '100%',
                height: '100%',
                timeAxis: {scale: 'minute', step: 15},
                /*orientation: {
                    axis: 'top',
                    item: 'top'
                },*/
                orientation: 'top',
                editable: {
                    updateGroup: true,
                    remove: true
                },
                template: itemContent,
                onMove: changeCategory,
                onRemove: removeItem,
                groupsOrder: false,
                groupOrder: false,
                showNavigation: true,
                max: maxMoment,
                min: minMoment/*,
                start: otherDate*/
            };

            // Create a Timeline
            timeline = new vis.Timeline(container, data, categories, options);

            // Thanks, hansmaulwurf23! https://github.com/almende/vis/issues/239
            var leftPanel = document.querySelector("div.vispanel.left");
            leftPanel.addEventListener("mousewheel", function(event) { // ver se tem como usar isso para navegar verticalmente nas categorias
                event.stopPropagation();
            }, true);

            /*var centerPanel = document.querySelector("div.vispanel.center");
            centerPanel.addEventListener("mousewheel", function(event) {
                if (event.wheelDelta < 0) // negative value for scolling down
                    document.style.cursor = "-webkit-zoom-out";
                else if (event.wheelDelta > 0)
                    centerPanel.style.cursor = "-webkit-zoom-in";
                else
                    centerPanel.style.cursor = "default";
                    
            });*/

            alertFunc();
            chrome.storage.sync.get('sort_by', function(order) {
                console.log("Preferência de ordenação do usuário: " + (order.sort_by + 1));
                if (order.length == 0 || order.sort_by === undefined) {
                    document.getElementById('selecao').value = 0;
                    // novaOrdenacao();
                } else {
                    document.getElementById('selecao').value = order.sort_by;
                    ordena(parseInt(order.sort_by));
                }
                
            });
            
            //console.log(JSON.stringify(categories[0]));
            // console.log("min" + categories[0].chromeId);
            // console.log(document.getElementById("min" + categories[0].chromeId).innerHTML);
            // document.getElementById("min" + categories[0].chromeId).addEventListener("click", MyFunction);
            //categories.forEach(function(elem) {
                //console.log(elem.chromeId + " " + elem.id);
                //document.getElementById("min" + elem.chromeId).innerHTML = "+";
                //document.getElementById("min" + elem.chromeId).onclick = MyFunction;
                //console.log(document.getElementById("min" + elem.chromeId).innerHTML);
            //});

        }
        

    });

});


// estamos usando o título da pasta como id da categoria :/
function addTreeNodeToTimeline (node, groupId) {

    // arrays que receberão os favoritos e as categorias já existentes
    var nodes = [];
    var groups = [];
    var oldest = null;

    // se o nodo é favorito, adicionamo-lo ao array

    if (node.url) {

        // id do favorito, nome do favorito com um link para a URL, data que foi adicionado, pastinha que se encontra
        //nodes[nodes.length] = {id: node.id, content: nodeContent(node), start: node.dateAdded, type:'point', group:groupId};
        nodes[nodes.length] = {
            //className: node.id + " w",
            //className: "w",
            //className: node.id,
            id: node.id,
            title: node.title, 
            url: node.url, 
            start: node.dateAdded, 
            group: groupId
        };

        oldest = new Date(node.dateAdded);

    }

    // se o nodo é uma pastinha, percorremos sua subárvore
    if (node.children) {
        if (node.title != "") {
            groups[groups.length] = { chromeId: node.id, title: node.title, id: node.title, content: node.title + 

            //console.log("Categoria " + node.id + ":" + node.title + " adicionada.");
                " <a id=\"min" + node.id + "\" title=\"Minimizar\" href=\"PleaseEnableJavascript.html\">[-]</a>" +
                // " <a id=\"min" + node.id + "\" title=\"Minimizar\" href=#></a>" +
                " <a id=\"exc" + node.id + "\"title=\"Excluir\" href=\"PleaseEnableJavascript.html\">[x]</a>"
            };
        }

        node.children.forEach(function (child) {
            // adiciona ao array todos os favoritos encontrados na subárvore
            var temp = addTreeNodeToTimeline(child, node.title);
            if ((oldest == null) || (temp.oldestBookmark != null && temp.oldestBookmark < oldest)) oldest = temp.oldestBookmark;
            nodes = nodes.concat(temp.bookmarks);
            groups = groups.concat(temp.categories);
        });
    }

    return {
        bookmarks: nodes,
        categories: groups,
        oldestBookmark: oldest
    };
}

function MyFunction() {
    // console.log("oi, " + JSON.stringify(parent));
    console.log("oi");
    return false;
}

function itemContent(item) {
  var content = 
    "<div class=\"w\" id=\"bookmark" + item.id + "\" title=\"Selecionar favorito\">" + 
    //"<div id=\"bookmark" + item.id + "\">" + 
      "<center>" +
        //"<img height='16' width='16' src=http://www.google.com/s2/favicons?domain=" + item.url + ">" +
        //"<img height='16' width='16' src=chrome://favicon/" + item.url + ">" +
        "<img height='16' width='16' src=chrome://favicon/" + item.url + " style=\"vertical-align: middle;\">" +
        "<a title=\"Abrir o favorito em nova guia\" href=" + item.url + " target=\"_blank\" style=\"display: inline-block; margin: 3px;\">" + 
        //"<a href=" + item.url + " target=\"_blank\" style=\"display: inline-block;\">" + 
          item.title.substring(0, 30) + 
        "</a>" + 
        "<div class=\"ep\" title=\"Arraste até outro favorito para criar uma relação\"></div>" + 
      "</center>" + 
    "</div>";

    return content;

}

function changeCategory(item, callback) {

    // alert(item.content + "\nitem.id = " + item.id + "\nitem.group = " + item.group);

    // encontra o nodo pai do favorito (categoria onde ele está)
    var result = categories.filter(function(element) {
        if (element.id == item.group) {
            // console.log('Achou. id = ' + element.chromeId);
            // console.log(JSON.stringify(element));
            return true; // não queria percorrer todo array :(
        }
    });
    console.log(result.length + " " + JSON.stringify(result));
    console.log(result[0].chromeId);
    
    // chrome.bookmarks.move(string id, object destination, function callback)
    chrome.bookmarks.move(item.id, { parentId: result[0].chromeId}, function () {
        // se tudo deu certo na API, callback(item) move o favorito na UI
        if (!chrome.runtime.lastError) {
            console.log('Favorito movido para a categoria ' + item.group);
            callback(item);
        } else {
            console.warn("Não foi possível mover o favorito para a categoria " + item.group + "\n\t\t" + chrome.runtime.lastError.message);
        }
    });

    instance.batch(function () {
        //instance.recalculateOffsets("lombrigas");
        instance.repaint("lombrigas");
    });

}

function removeItem(item, callback) {

  var r = confirm("O favorito e suas relações serão removidos da timeline. Tudo bem?");

  if (r == true) { // confirmado pelo usuário

    var removed = 0;

    // procurar relações preexistentes para deleção
    instance.select({source: "bookmark" + item.id}).each( function(connection) {
        
        // console.log(JSON.stringify(connection.id) + "será removida");

        instance.detach(connection);

        //minhocas[connection.id] = null;
        //delete minhocas[connection.id]; // não sei se é a forma correta de remover um 
                                        // campo de um objeto, mas foi a única que encontrei

        // console.log('connections = ' + JSON.stringify(Object.keys(minhocas)));
        
        // instance.detach(connection);
        removed++;

    });

    instance.select({target: "bookmark" + item.id}).each(function(connection) {
        
        // console.log(JSON.stringify(connection.id) + "será removida");
        
        //minhocas[connection.id] = null;
        //delete minhocas[connection.id];

        // console.log('connections = ' + JSON.stringify(Object.keys(minhocas)));
        
        instance.detach(connection);
        removed++;

    });

    // salvar de volta no storage
    //chrome.storage.sync.set({'connections': JSON.stringify(minhocas)}, function() {
    //    if (!chrome.runtime.lastError) {
            
            // remover o favorito do Chrome
            chrome.bookmarks.remove(item.id, function () {
                console.log("Um favorito e " + removed + " relações excluídos.");
            })

            // remover da tela
            callback(item);

            instance.batch(function () {
              // instance.recalculateOffsets("lombrigas");
              instance.repaint("lombrigas");
            });
        //}
        //else {
        //    console.warn("Não foi possível excluir as relações do favorito\n\t\t" + chrome.runtime.lastError.message);
        //    callback(null);
        //}
    //});

  } else {
    callback(null);
  }

}

function novaOrdenacao() {
    var menu = document.getElementById('selecao')
    var op = menu.selectedIndex;

    chrome.storage.sync.set({'sort_by': op}, function() {
        // console.log("Relação " + minhocas[key] + " salva! ");
        console.log('Preferência de ordenação salva: ' + (op + 1) + " - " + menu.options[op].text);
    });

    ordena(op);
}

function ordena(sortPref) {
   
    if (sortPref == 0) { // Padrão do browser
        browserOrder();
    } else if(sortPref == 1) { // Alfabeticamente
        categories.sort(function (a, b) {
        return (a.content > b.content) ? 1 : ((b.content > a.content) ? -1 : 0);
    });
    timeline.setGroups(categories);

    } else if (sortPref == 2) { // Abertas primeiro
        visibleFirst();
    }

    instance.batch(function () {
        // console.log("Repintando lombrigas");
        // instance.recalculateOffsets("lombrigas");
        instance.repaint("lombrigas");
    });

}

function browserOrder() {
  chrome.bookmarks.getTree(function (results) {
    categories = browserCategories(results[0]);
    timeline.setGroups(categories);
  });

}

function browserCategories(node) {

  var groups = new Array();

  if (!(node.url)) {
    if (node.title != "")
      groups.push({ chromeId: node.id, id: node.title, content: node.title });
    if (node.children) {
      node.children.forEach(function (child) {
        groups = groups.concat(browserCategories(child));
      });
    }
  }

  return groups;
}

function visibleFirst() {
  // Get an array with the ids of the currently visible items
  var visibleItems = timeline.getVisibleItems();

  var visibleGroups = new Set();

  visibleItems.forEach( function (itemId) {
    // String com o nome da categoria
    visibleGroups.add(data.get(itemId).group);
    //console.log(data.get(itemId).group);
  });

  var openCategories = new Array();
  var closedCategories = new Array();

  categories.forEach(function (categ) {
    // console.log(categ);

    var index = 0;
    if (visibleGroups.has(categ.content)) {
      openCategories[openCategories.length] = categ;
    } else {
      closedCategories[closedCategories.length] = categ;
    }
    index++;
  });
  
  categories = openCategories.concat(closedCategories);

  timeline.setGroups(categories);

}

function showReport() { // mostrar também a quantidade de favoritos?

    var items = new vis.DataSet();
    var groups = new vis.DataSet();

    var dialog = document.getElementById("myDialog");

    dialog.showModal();

    var chart = document.getElementById("reportChart");

    //var today = new Date().toJSON().slice(0, 10);

    var today = new Date();

    //console.log(today.getTimezoneOffset()); // +2:00
    //console.log(today.getHours() + ":" + today.getMinutes()); // mas a hora aparece certa
    
    var future = new Date();
    var past = new Date();
    future.setDate(today.getDate() + 2);
    past.setDate(today.getDate() - 1);

    chrome.storage.sync.get('access_count', function(report) {

        // console.log(JSON.stringify(report));

        if (report != null && report !== undefined) {
            var acc = report.access_count;

            groups.add({id: 0, content: "Anual: " + acc.annually});
            groups.add({id: 1, content: "Mensal: " + acc.monthly});
            groups.add({id: 2, content: "Hoje: " + acc.daily});

            items.add({x: today, y: acc.annually, group: 0});
            items.add({x: today, y: acc.monthly, group: 1});
            items.add({x: today, y: acc.daily, group: 2});

        } else {
            console.warn('Não há dados de acesso disponíveis no momento.');
            // desabilitar gráfico
        }
    });

    // var dataset = new vis.DataSet(items);

    var options = {
        orientation: {
            axis: 'none'
        },
        showCurrentTime: false, 
        style: 'bar',
        stack: false,
        barChart: { width: 30, align: 'center', sideBySide: false },
        drawPoints: false,
        start: past,
        end: future,
        width:  '320px',
        height: '300px',
        graphHeight: '250px',
        moveable: false,
        zoomable: false,
        legend: {
            enabled: true,
            left: {
                position: 'top-right'
            }
        },
        dataAxis: {
            left: {
                range: { min: 0 },
                format: function (value) {
                    // console.log(value);
                    return groups.get(value).content;
                }
            }
        },
        locales: {
            // create a new locale
            pt_br: {
              current: 'atual',
              time: 'hora',
              june: 'junho'
            }
          },
        locale: 'pt_BR'
    };

    var graph2d = new vis.Graph2d(chart, items, groups, options);

    // é necessário destruir o gráfico ou ele aparece várias vezes
    document.getElementById("closeReport").graphToDestroy = graph2d;
    document.getElementById("closeReport").addEventListener("click", closeReport);    
    // outra alternativa seria exibir sempre o mesmo?

    return false;

}

function closeReport(evt) {

    evt.target.graphToDestroy.destroy();
    document.getElementById("closeReport").removeEventListener("click", closeReport);
    document.getElementById("myDialog").close();

}